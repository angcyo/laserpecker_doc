import { defineConfig } from "vitepress";
import { defaultConfig } from "./defaultConfig";
import sidebar from "./config/sidebar";

const base = "/laserpecker_doc/";

export default defineConfig({
  title: "LaserPecker",
  titleTemplate: "Vite & Vue powered static site generator",
  base: defaultConfig.baseUrl,
  locales: {
    "/": {
      lang: "en-US",
    },
    "/zh/": {
      lang: "zh-CN",
    },
  },
  themeConfig: {
    sidebar,
    siteTitle: "LaserPecker",
    footer: {
      message: "LaserPecker",
      copyright: "Copyright © 2022-present",
    },
    localeLinks: {
      text: "",
      items: [
        { text: "简体中文", link: "/zh/" },
        { text: "English", link: "/" },
      ],
    },
  },
  markdown: {
    theme: "dracula",
    config: (md) => {},
  },
  vite: {},
});
